// This is the client side script. This listens for chat-app.js, which is the backend script. This file is included in the client side's HTML.

$(function () {
    const socket = io.connect('http://localhost:3000');

    // const message = $('#message');
    const userInputBox = $('#userInputBox');
    const sendButton = $('#sendButton');
    const chatroom = $('#chatContainerDiv');

    // receive message from server
    socket.on('message', (data) => {
        chatroom.append('<p id="messsageSpan">' + data + '</p>');
        chatroom[0].scrollTop = chatroom[0].scrollHeight; // scroll to the bottom every time new message gets displayed
    });

    // send message to server
    sendButton.click(function () {
        socket.emit('user_message', {
            message: userInputBox.val()
        });
        userInputBox.val('');
        userInputBox.focus();
    });

    userInputBox.on('keyup', (e) => {
        if (e.key === 'Enter') { // when enter key pressed
            sendButton.click();
        }
    });
});

function emptyMessage() {}