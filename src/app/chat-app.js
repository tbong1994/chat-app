// This is the back end script. Socket.io object sends messages/whatever to the client side, which is listened by chat.js

const express = require('express');
const app = express();
const http = require('http');
const debug = require('debug')('chat-app:server');
const path = require('path');

// app.use(express.static('public'));
app.use(express.static(path.join(__dirname, "./../views")));
app.use(express.static(path.join(__dirname, "./../public")));
app.set('views', path.join(__dirname, './../views'));
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/../views/index.html');
    // res.render('index');
});


// dev port
const port = normalizePort(3000);

// prod port
// const port = proess.env.PORT ;
// server = app.listen(port);

// create server
const server = http.createServer(app, (req, res) => {
    res.end('server listening on : ' + port);
}).listen(port);

// listen to server
server.on('error', onError);
server.on('listening', onListening);

// instantiate a socket
const io = require('socket.io')(server); // create socket on current server

// listen to every connection
io.on('connection', (socket) => {

    // new user is connected. Send message
    socket.emit('message', 'Write whatever you want!');
    socket.username = 'user ' + io.sockets.clients.length;

    // receive messages from user and resend the message to ALL CLIENTS to display on the chat box
    socket.on('user_message', (data) => {
        io.sockets.emit('message', socket.username + ' : ' + data.message); // emit to all connected sockets
    });
});

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
    debug('Listening on ' + bind);
}